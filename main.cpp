/* CS3241R Project
 * Ewald Hew A0155087Y
 * ===================
 * This project builds upon the Lab 5 assignment to add better lighting
 * effects.
 */
#include "main.h"
#include "objects.h"
#include "raytracer.h"
#include "photons.h"

using namespace std;

#define WINWIDTH 600
#define WINHEIGHT 600

float* pixelBuffer = new float[WINWIDTH * WINHEIGHT * 3];

// Globals
RtObject **objList; // The list of all objects in the scene
SceneVars vars;
KDTreeNode *causticPhotonMap;
int sceneNo = 1;


bool unproject( int& x, int& y, Vector3 const& pos )
{
    Vector3 point = vars.cameraPos - pos;
    Vector3 focus = vars.lookAtDir * dot_prod( point, vars.lookAtDir );
    Vector3 offst = point - focus;
    double length = offst.length();
    offst.normalize();
    Vector3 unpro = offst * (length / focus.length() * vars.focalLen);

    assert( unpro.x[2] == 0 );
    x = floor( WINWIDTH / 2.0 - unpro.x[0] );
    y = floor( WINHEIGHT/ 2.0 - unpro.x[1] );
    if( x < 0 || y < 0 || x >= WINWIDTH || y >= WINHEIGHT )
    {
        x = y = 0;
        return false;
    }
    return true;
}

void drawInPixelBuffer(int x, int y, double r, double g, double b)
{
    pixelBuffer[(y*WINWIDTH + x) * 3] = (float)r;
    pixelBuffer[(y*WINWIDTH + x) * 3 + 1] = (float)g;
    pixelBuffer[(y*WINWIDTH + x) * 3 + 2] = (float)b;
}

bool drawPoint( Vector3 const& pos, double r, double g, double b )
{
    int x, y;
    bool onScreen = unproject( x, y, pos );
    if ( onScreen )
        drawInPixelBuffer( x, y, r, g, b );

    return onScreen;
}

void display( void );

void renderScene()
{
    int x, y;
    Ray ray;
    double r, g, b;

    cout << "Rendering Scene " << sceneNo << " with resolution " << WINWIDTH << "x" << WINHEIGHT << "........... ";
    __int64 time1 = chrono::duration_cast<chrono::milliseconds>(chrono::steady_clock::now().time_since_epoch()).count(); // marking the starting time

    /* Photon Map Generation */
    KDTreeNode map = {};
    makePhotonMap(&map);
    causticPhotonMap = &map;
    display();

    /* Render Pass */
    ray.start = vars.cameraPos;

    Vector3 vpCenter = vars.cameraPos + vars.lookAtDir * vars.focalLen;  // viewplane center
    Vector3 startingPt = vpCenter + vars.leftVector * (-WINWIDTH / 2.0) + vars.upVector * (-WINHEIGHT / 2.0);
    Vector3 currPt;

    for ( x = 0; x < WINWIDTH; x++ )
    {
        for ( y = 0; y < WINHEIGHT; y++ )
        {
            currPt = startingPt + vars.leftVector*x + vars.upVector*y;
            ray.dir = currPt - vars.cameraPos;
            ray.dir.normalize();
            rayTrace( ray, r, g, b, SET_ALL );
            drawInPixelBuffer( x, y, r, g, b );
        }
        if ( x % 20 == 0 )
            display();
    }

    __int64 time2 = chrono::duration_cast<chrono::milliseconds>(chrono::steady_clock::now().time_since_epoch()).count(); // marking the ending time

    cout << "Done! \nRendering time = " << time2 - time1 << "ms" << endl << endl;
}

void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT |GL_DOUBLEBUFFER);
    glDrawPixels(WINWIDTH, WINHEIGHT, GL_RGB, GL_FLOAT, pixelBuffer);
    glutSwapBuffers();
    glFlush ();
}

void reshape (int w, int h)
{
    glViewport (0, 0, (GLsizei) w, (GLsizei) h);

    glMatrixMode (GL_PROJECTION);
    glLoadIdentity();

    glOrtho(-10, 10, -10, 10, -10, 10);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}


void setScene(int i = 0)
{
    if (i > NUM_SCENE)
    {
        cout << "Warning: Invalid Scene Number" << endl;
        return;
    }

    if (i == 0)
    {
        objList[0]->init( Vector3( -130, 80, 120 ), 100, {
            { 0.1, 0.4, 0.4 },
            { 0.0, 1.0, 1.0 },
            { 0.2, 0.4, 0.4 }
        } );

        objList[1]->init( Vector3( 130, -80, -80 ), 100, {
            { 0.6, 0.6, 0.2 },
            { 1.0, 1.0, 0.0 },
            { 0.0, 0.0, 0.0 },
            50
        } );

        objList[2]->init( Vector3( -130, -80, -80 ), 100, {
            { 0.1, 0.6, 0.1 },
            { 0.1, 1.0, 0.1 },
            { 0.3, 0.7, 0.3 },
            650
        } );

        objList[3]->init( Vector3( 130, 80, 120 ), 100, {
            { 0.3, 0.3, 0.3 },
            { 0.7, 0.7, 0.7 },
            { 0.6, 0.6, 0.6 },
            650
        } );

        objList[4]->visible = false;
        objList[5]->visible = false;
        objList[6]->visible = false;
    }

    if (i == 1)
    {
        objList[0]->init( Vector3( -100, 180, 50 ), 50, {
            { 0.1, 0.4, 0.4 },
            { 0.0, 1.0, 1.0 },
            { 0.2, 0.4, 0.4 }
        } );

        objList[1]->init( Vector3( 80, -700, 600 ), 600, {
            { 0.6, 0.6, 0.2 },
            { 1.0, 1.0, 0.0 },
            { 0.0, 0.0, 0.0 },
            50
        } );

        objList[2]->init( Vector3( 200, -50, 50 ), 100, {
            { 0.1, 0.6, 0.1 },
            { 0.1, 1.0, 0.1 },
            { 0.3, 0.7, 0.3 },
            650
        } );

        objList[3]->init( Vector3( -30, 50, 120 ), 80, {
            { 0.6, 0.6, 0.6 },
            { 0.7, 0.7, 0.7 },
            { 0.6, 0.6, 0.6 },
            650
        }, bump1 );

        objList[4]->init( Vector3( -55, -25, -200 ), 50, {
            { 0.0, 0.0, 0.0 },
            { 0.1, 0.1, 0.1 },
            { 1.0, 1.0, 1.0 },
            650, .97
        } );

        objList[5]->init( vars.leftVector, -300, {
            { 0.6, 0.6, 0.6 },
            { 1.0, 1.0, 1.0 },
            { 0.0, 0.0, 0.0 },
            50
        } );

        objList[6]->init( Vector3( 0, 10, 1 ), 999, {
            { 0.1, 0.1, 0.1 },
            { 0.3, 0.3, 0.3 },
            { 0.8, 0.8, 0.8 },
            650, .99
        }, bump2 );
        objList[6]->visible = false;
    }

}

void keyboard (unsigned char key, int x, int y)
{
    //keys to control scaling - k
    //keys to control rotation - alpha
    //keys to control translation - tx, ty
    switch (key) {
    case 's':
    case 'S':
        sceneNo = (sceneNo + 1 ) % NUM_SCENE;
        setScene(sceneNo);
        renderScene();
        glutPostRedisplay();
        break;
    case 'q':
    case 'Q':
        exit(0);

        default:
        break;
    }
}

int main(int argc, char **argv)
{


    cout<<"<<Raytracer>>\n\n"<<endl;
    cout << "S to go to next scene" << endl;
    cout << "Q to quit"<<endl;
    glutInit(&argc, argv);
    glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowSize (WINWIDTH, WINHEIGHT);


    glutCreateWindow ("CS3241R: Ray Tracing");

    glutDisplayFunc(display);
    glutReshapeFunc(reshape);

    glutKeyboardFunc(keyboard);

    objList = new RtObject*[NUM_OBJECTS];
    objList[0] = new Sphere();
    objList[1] = new Sphere();
    objList[2] = new Sphere();
    objList[3] = new Sphere();
    objList[4] = new Sphere();
    objList[5] = new Plane();
    objList[6] = new Plane();
    objList[7] = new Sphere();

    // set scene view and lighting
    vars = {
        Vector3( 0, 0, -500 ), // cameraPos
        Vector3( 0, 0, 1 ),    // lookAtDir
        // assume the the following two vectors are normalised
        Vector3( 0, 1, 0 ),        // upVector
        Vector3( 1, 0, 0 ),        // leftVector

        Vector3( 900, 1000, -1500 ), // lightPos
        { 0.4, 0.4, 0.4 },             // ambiantLight
        { 0.7, 0.7, 0.7 },            // diffuseLight
        { 0.5, 0.5, 0.5 },            // specularLight

        { 0.1, 0.1, 0.4 }, // bgColor

        500 // focalLen
    };

    setScene(0);

    setScene(sceneNo);
    renderScene();

    glutMainLoop();

    for (int i = 0; i < NUM_OBJECTS; i++)
        delete objList[i];
    delete[] objList;

    delete[] pixelBuffer;

    return 0;
}
