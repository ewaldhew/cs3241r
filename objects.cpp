#include "objects.h"

void bump1( Vector3 const& intersection, Vector3& normal )
{
    double a1 = acos( normal.x[1] );
    double a2 = atan2( normal.x[2], normal.x[0] );
    double offset = 0.07*sin( 20 * (a1 - a2) )*sin( 20 * (a1 + a2) );

    normal.x[0] += offset;
    normal.x[1] += offset;

    normal.normalize();
}

void bump2( Vector3 const& intersection, Vector3& normal )
{
    double offset = sin( 0.1*intersection.x[2] );

    normal.x[0] += offset;
    normal.x[1] += offset;

    normal.normalize();
}

void bump3( Vector3 const& intersection, Vector3& normal )
{
    double offset = eval( intersection.x[1] / 20, intersection.x[0] / 20 );

    normal.x[0] += offset;
    normal.x[1] += offset;

    normal.normalize();
}


double Sphere::intersectWithRay( Ray r, Vector3& intersection, Vector3& normal )
// return a -ve if there is no intersection. Otherwise, return the smallest postive value of t
{

    // Step 1
    Vector3 offset = r.start - center_;

    //     a = 1.0
    double b = 2 * dot_prod( r.dir, offset );
    double c = offset.lengthsqr() - r_*r_;
    double d = b*b - 4 * c;

    if (d > 0)
    {
        double p = -b * 0.5;
        double q = sqrt( d ) * 0.5;
        double t;

        if (p - q > 0) t = p - q;
        else if (p + q > 0) t = p + q;
        else return -1;

        intersection = r.start + r.dir*t;
        normal = (intersection - center_);
        normal.normalize();

        if ( bumpMap )
            bumpMap( intersection, normal );

        return t;
    }

    return -1;
}
bool Sphere::intersects( Ray r )
{
    Vector3 offset = r.start - center_;

    //     a = 1.0
    double b = 2 * dot_prod( r.dir, offset );
    double c = offset.lengthsqr() - r_*r_;
    double d = b*b - 4 * c;

    return ((d > 0) &&
        (b < 0 || 4 * c < 0));
}
double Sphere::getRadius( Vector3 const& from )
{
    return atan2( r_, (center_ - from).length() );
}

double Sphere::intersectWithCone( Cone c )
{
    Vector3 offset = center_ - c.center.start;
    Vector3 T = c.center.dir * dot_prod( offset, c.center.dir );

    if ( dot_prod( T, c.center.dir ) < 0 ) // Avoid back of cone being occluded
        return 0.0;

    double S = (offset - T).length();
    double R = T.length()*c.tan_a;
    double r = r_*c.inv_cos_a;

    if ( r + R <= S )
        return 0.0;
    else
    {
        double h = r + R - S;
        if ( 2.0*r < h )
            return 1.0;
        else
        {
            double k = h / R;
            if ( k > 2.0 ) k = 2.0;
            return (acos( 1 - k ) - (1 - k)*sqrt( 2*k - k*k )) / M_PI;
        }
    }
}



double Plane::intersectWithRay( Ray r, Vector3& intersection, Vector3& normal )
{
    // l(t) = r.start + t * r.dir
    // so l(t) . n = d r.start.n + t * r.dir.n = d
    // t = (d-r.start dot n)/(r.dir dot n)
    double t = (d_ - dot_prod( r.start, n_ )) / (dot_prod( r.dir, n_ ));
    intersection = r.start + r.dir * t;
    if (intersection.x[2] > -1000 &&
         intersection.x[2] < 1000)
    {
        if (dot_prod( r.dir, n_ ) > 0)
            normal = -n_;
        else
            normal = n_;

        if ( bumpMap )
            bumpMap( intersection, normal );

        normal.normalize();

        return t;
    }
    return -1;
}
bool Plane::intersects( Ray r )
{
    return false;
}
double Plane::intersectWithCone( Cone )
{
    return 0.0;
}
double Plane::getRadius( Vector3 const& from )
{
    return atan2( 1000, (getCenter() - from).length() );
}
