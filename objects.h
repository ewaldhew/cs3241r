#ifndef OBJECTS_H_
#define OBJECTS_H_

#include "vector3D.h"
#include "main.h"
#include "raytracer.h"
#include "OpenSimplexNoise.h"

typedef void( *BumpMapFunc )(Vector3 const&, Vector3&);

struct RtPptys {
    double ambiantReflection[3];
    double diffuseReflection[3];
    double specularReflection[3];
    double speN = 300;
    double opticalSpd = -1;
};

class RtObject {

public:
    virtual void init( Vector3 pos, double param, RtPptys pptys = {}, BumpMapFunc bumpCallback = NULL ) = 0;

    virtual double intersectWithRay( Ray, Vector3& pos, Vector3& normal ) = 0; // return a -ve if there is no intersection. Otherwise, return the smallest postive value of t
    virtual bool intersects( Ray ) = 0;

    virtual double intersectWithCone( Cone ) = 0; // returns the fractional coverage with this coneray

    virtual Vector3 getCenter() = 0;
    virtual double getRadius( Vector3 const& from ) = 0;

    inline bool isSpecular() {
        return pptys.specularReflection[0] > 0 ||
               pptys.specularReflection[1] > 0 ||
               pptys.specularReflection[2] > 0;
    };
    inline bool isTransparent() {
        return pptys.opticalSpd > 0;
    };

    bool visible = false;
    // Materials Properties
    RtPptys pptys;
};

class Sphere : public RtObject {

    Vector3 center_;
    double r_;
    BumpMapFunc bumpMap;
public:
    Sphere( Vector3 c, double r ) { center_ = c; r_ = r; };
    Sphere() {};

    void init( Vector3 pos, double param, RtPptys pptys = {}, BumpMapFunc bumpCallback = NULL )
    { center_ = pos; r_ = param; visible = true; this->pptys = pptys; bumpMap = bumpCallback; };

    double intersectWithRay( Ray, Vector3& pos, Vector3& normal );
    bool intersects( Ray );
    /** Gets the percent coverage on the given cone */
    double intersectWithCone( Cone );

    Vector3 getCenter() { return center_; };
    double getRadius( Vector3 const& );
};

class Plane : public RtObject {
    // the plane of any point v such that v . n = d
    Vector3 n_;
    double d_;
    BumpMapFunc bumpMap;
public:
    Plane( Vector3 n, double d ) { n_ = n; d_ = d; };
    Plane() {};

    void init( Vector3 pos, double param, RtPptys pptys = {}, BumpMapFunc bumpCallback = NULL )
    { n_ = pos; d_ = param; visible = true; this->pptys = pptys; bumpMap = bumpCallback ; };

    double intersectWithRay( Ray, Vector3& pos, Vector3& normal );
    /* Always returns false */
    bool intersects( Ray );
    /* Always returns zero */
    double intersectWithCone( Cone );

    Vector3 getCenter() { return n_ * d_; };
    double getRadius( Vector3 const& );
};

void bump1( Vector3 const& intersection, Vector3& normal );
void bump2( Vector3 const& intersection, Vector3& normal );
void bump3( Vector3 const& intersection, Vector3& normal );

#endif // !OBJECTS_H_
