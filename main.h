#ifndef MAIN_H_
#define MAIN_H_

#include <cmath>
#include <iostream>
#include <vector>
#include <queue>
#include "GL\freeglut.h"
#include "vector3D.h"
#include <chrono>

#define NUM_OBJECTS 8
#define MAX_RT_LEVEL 50
#define NUM_SCENE 2

#define SHADOW_BUNDLE_COUNT 10
#define SHADOW_BUNDLE_ROUNDS 5
#define EPSZ 0.000000001
#define M_PI 3.14159265359

#define DEBUG_
#ifdef DEBUG_
#define assert(x) if (!(x)) *(int *)0 = 0;
#else
#define assert(x)
#endif

// forward declaration
class RtObject;

struct SceneVars {
    Vector3 cameraPos;
    Vector3 lookAtDir;
    Vector3 upVector;
    Vector3 leftVector;

    Vector3 lightPos;
    double ambiantLight[3];
    double diffuseLight[3];
    double specularLight[3];

    double bgColor[3];

    float focalLen;
};

bool drawPoint( Vector3 const&, double r=1.0, double g=1.0, double b=1.0 );

#endif // !MAIN_H_
