#ifndef PHOTONS_H_
#define PHOTONS_H_

#include "main.h"
#include "vector3D.h"

#define MAX_PHOTONS 500000

typedef struct Photon_ {
    char dir_t;
    char dir_p;
    float e_r;
    float e_g;
    float e_b;
    bool flag;
    Vector3 pos;
} *Photon, *KDTreeHeapNode;

struct KDTreeNode {
    int k;
    KDTreeNode* left;
    KDTreeNode* right;
    Photon      data;

    KDTreeNode() {
        k = -1;
        left = right = 0;
        data = 0;
    }
};
typedef std::vector<KDTreeHeapNode> KDTreeHeap;


struct HeapRec {
    Photon photon;
    double distSq;

    HeapRec( Photon photon, double distSq )
        : photon( photon ), distSq( distSq ) { };
};

class PhotonGatherHeap {
public:
    void insert( Photon photon, double distSq )
    {
        internalHeap.emplace( photon, distSq );
        if ( internalHeap.size() > size ) internalHeap.pop();
        assert( internalHeap.size() <= size );
    };
    bool isFull() { return internalHeap.size() == size; };
    double getMaxDistSq() { return internalHeap.top().distSq; };
    std::vector<Photon> getList() { return internalHeap.getList(); };

    PhotonGatherHeap( unsigned int size )
    {
        internalHeap = PhotonGatherHeapInternal();
        this->size = size;
    };

private:
    class comp {
    public:
        bool operator() ( HeapRec const& a, HeapRec const& b ) const {
            return a.distSq < b.distSq;
        }
    };

    class PhotonGatherHeapInternal : public std::priority_queue<HeapRec, std::vector<HeapRec>, comp> {
    public:
        std::vector<Photon> getList()
        {
            std::vector<Photon> result;
            for ( HeapRec p : c ) result.push_back( p.photon );
            return result;
        };
    };

    unsigned int size;
    PhotonGatherHeapInternal internalHeap;
};


void insertPhoton( std::vector<Photon>& photons,
                   Vector3 pos,
                   Vector3 const& dir,
                   Vector3 const& energy );

void constructKDTree( KDTreeNode* root,
                      std::vector<Photon>::iterator begin,
                      std::vector<Photon>::iterator end );

void constructKDTreeHeap( KDTreeHeap& root,
                          int idx,
                          std::vector<Photon>::iterator begin,
                          std::vector<Photon>::iterator end );

void makePhotonMap( KDTreeNode* map );

void estimateRadiance( KDTreeNode* map, Vector3 const& pos, double& r, double& g, double& b );

#endif
