#include "raytracer.h"
#include "objects.h"


inline static double getShadow( int obj, Vector3 location, Vector3 light, double distLight )
{
    Ray test;
    test.start = location;
    test.dir = light;
    double result = 1.0;
#if 0 /* Old shadow calculation routine - at least 10x slower! */
    double step = result / (SHADOW_BUNDLE_COUNT*SHADOW_BUNDLE_ROUNDS);

    for (int s = 0; s < SHADOW_BUNDLE_COUNT; s++)
    {
        double a1 = 6.28318531 * s / SHADOW_BUNDLE_COUNT;

        for (int k = SHADOW_BUNDLE_ROUNDS; k > 0; k--)
        {
            double a2 = atan2( k*20.0, distLight );

            test.dir = rotate(light, a1, a2);

            for (int j = 0; j < NUM_OBJECTS; j++)
            {
                if (j != obj &&
                     objList[j]->visible &&
                     objList[j]->intersects( test ))
                {
                    if ( !(objList[j]->isTransparent()) )
                        result -= step;
                    else
                        result -= 0.5*objList[j]->pptys.opticalSpd*step;
                    break;
                }
            }
        }
    }
#else
    Cone lightCone = Cone( test, atan2( 70.0, distLight ) );

    for ( int j = 0; j < NUM_OBJECTS; j++ )
    {
        if ( j != obj && objList[j]->visible )
        {
            if ( !(objList[j]->isTransparent()) )
                result -= objList[j]->intersectWithCone( lightCone );
        }
    }
#endif

    return (result < 0) ? 0 : result;
}

inline static int getFirstHit( Ray ray, Vector3& intersection, Vector3& normal, double& dist, int fromObj, bool testSelf = false )
{
    int hitObj = -1;
    double mint = DBL_MAX;
    Vector3 temp;

    for (int i = 0; i < NUM_OBJECTS; i++)
    {
        if ( (!testSelf && i == fromObj) || !objList[i]->visible )
            continue;

        if ((dist = objList[i]->intersectWithRay( ray, temp, temp )) > EPSZ)
        {
            if (mint < dist)
            {
                continue;
            }
            else
            {
                hitObj = i;
                mint = dist;
            }
        }
    }

    if (hitObj != -1)
    {
        dist = objList[hitObj]->intersectWithRay( ray, intersection, normal );
    }
    return hitObj;
}


static void photonTrace( std::vector<Photon>& photons, Ray photonRay, double r, double g, double b )
{
    int level = 0;
    int lastHitObj = -1;

    bool hasSpecularBounce = false;

    Vector3 intersection, normal;
    double distFromLast;

    while ( level <= MAX_RT_LEVEL )
    {
        int i = getFirstHit( photonRay, intersection, normal, distFromLast, lastHitObj, true );
        if ( i == -1 ) return;

        lastHitObj = i;

        double energyFactor = 1.0 / (1.0 + 0.0000001*distFromLast*distFromLast);
        double angleFactor = dot_prod( -photonRay.dir, normal );

        // Spectrum shift
        //r *= objList[i]->pptys.ambiantReflection[0];
        //g *= objList[i]->pptys.ambiantReflection[1];
        //b *= objList[i]->pptys.ambiantReflection[2];

        // Store on diffuse (non-transparent) surface, for photons
        // with at least 1 specular intersection, and terminate.
        if ( !objList[i]->isTransparent() )
        {
            if ( hasSpecularBounce )
                insertPhoton( photons, intersection, -photonRay.dir, Vector3( r, g, b ) );
            return;
        }
        // Store on diffuse surface and continue - for global illum.
        // Russian roulette on specular surface - reflect, transmit, or absorb.
        else
        {
            double rnd = (double)rand() / RAND_MAX;
            double a = .0;
            double b = 1.0;
            double c = 1.0;
            double attn[3];

            int choice = 0;
            if ( a < rnd && rnd <= b ) choice = 1;
            else if ( b < rnd && rnd <= c ) choice = 2;

            hasSpecularBounce = true;
            photonRay.start = intersection;

            switch ( choice ) {
            case 0:
                // reflected
                if ( angleFactor > EPSZ && objList[i]->isSpecular() )
                {
                    Vector3 rayReflectionV = normal * 2 * angleFactor - (-photonRay.dir);
                    photonRay.dir = rayReflectionV;
                    //photonTrace( photons, refl, r, g, b, i, level + 1 );
                }
                break;
            case 1:
            {
                // refracted
                double ts = objList[i]->pptys.opticalSpd;
                if ( ts > EPSZ )
                {
                    Vector3 transmisV;

                    if ( angleFactor > EPSZ ) //into
                    {
                        transmisV = normal * (ts*angleFactor - sqrt( 1 - ts*ts*(1 - angleFactor*angleFactor) ))
                            - (-photonRay.dir) * ts;
                    }
                    else //out
                    {
                        double invts = 1.0 / ts;
                        double det = 1 - invts*invts*(1 - angleFactor*angleFactor);
                        if ( det < 0 ) return;
                        transmisV = normal * (-invts*angleFactor - sqrt( det )) - (-photonRay.dir) * invts;
                    }
                    transmisV.normalize();
                    photonRay.dir = transmisV;
                    //rayTrace( trns, tr, tg, tb, m, i, level + 1 );

                    double cAngle = angleFactor > 0 ? angleFactor : dot_prod( transmisV, normal );
                    double R0 = ((ts - 1)*(ts - 1)) / ((ts + 1)*(ts + 1));
                    attn[0] =
                        attn[1] =
                        attn[2] = R0 + (1 - R0)*pow( 1 - cAngle, 5 );

                    //r += (1 - attn[0])*tr;
                    //g += (1 - attn[1])*tg;
                    //b += (1 - attn[2])*tb;
                }
                else
                {
                    attn[0] = objList[i]->pptys.specularReflection[0];
                    attn[1] = objList[i]->pptys.specularReflection[1];
                    attn[2] = objList[i]->pptys.specularReflection[2];
                }

                //r += attn[0] * rr;
                //g += attn[1] * rg;
                //b += attn[2] * rb;
                break;
            }
            case 2:
                // absorbed
                return;
            }

            photonRay.start = photonRay.start + photonRay.dir * 0.0001;
            level++;
        }

    }
}

static void emitPhotons( std::vector<Photon>& photons, RtObject* towards )
{
    for (int i = 0; i < MAX_PHOTONS; i++)
    {
        Ray photonRay = { vars.lightPos, randCone( towards->getCenter() - vars.lightPos,
                                                   3*towards->getRadius( vars.lightPos ) ) };
        photonTrace( photons, photonRay, vars.specularLight[0], vars.specularLight[1], vars.specularLight[2] );
    }
}



void emitCausticPhotons( std::vector<Photon>& photons )
{
    for (int i = 0; i < NUM_OBJECTS; i++)
    {
        if ( objList[i]->visible &&
             objList[i]->isTransparent() )
        {
            emitPhotons( photons, objList[i] );
        }
    }
}


void rayTrace( Ray ray, double& r, double& g, double& b, char mask, int fromObj, int level )
{
    // Step 4
    if (level > MAX_RT_LEVEL) return;

    int i = 0;

    Vector3 intersection, normal;

    double mint = DBL_MAX;

    bool setR = mask & SET_RED;
    bool setG = mask & SET_GRN;
    bool setB = mask & SET_BLU;

    i = getFirstHit( ray, intersection, normal, mint, fromObj );

    if (i == -1)
    {
        if (setR) r = vars.bgColor[0];
        if (setG) g = vars.bgColor[1];
        if (setB) b = vars.bgColor[2];
    }
    else
    {
        if (setR) r = objList[i]->pptys.ambiantReflection[0] * vars.ambiantLight[0];
        if (setG) g = objList[i]->pptys.ambiantReflection[1] * vars.ambiantLight[1];
        if (setB) b = objList[i]->pptys.ambiantReflection[2] * vars.ambiantLight[2];

        Vector3 viewV = -ray.dir;
        Vector3 lightV = vars.lightPos - intersection;
        double distLightSq = lightV.lengthsqr();
        double d = 1.0 / (1.0 + 0.00000001*distLightSq);
        lightV.normalize();
        double ln = dot_prod( lightV, normal );

        Vector3 lightReflectionV = normal * 2 * ln - lightV;
        double spec = dot_prod( lightReflectionV, viewV );
        double speP = pow( spec, objList[i]->pptys.speN );


        // shadows
        double lightAmt = getShadow( i, intersection, lightV, sqrt( distLightSq ) );

        // local light
        if (ln > EPSZ)
        {
            d *= lightAmt;

            if (setR) r += d * objList[i]->pptys.diffuseReflection[0] * vars.diffuseLight[0] * ln;
            if (setG) g += d * objList[i]->pptys.diffuseReflection[1] * vars.diffuseLight[1] * ln;
            if (setB) b += d * objList[i]->pptys.diffuseReflection[2] * vars.diffuseLight[2] * ln;

            if (spec > EPSZ)
            {
                if (setR) r += d * objList[i]->pptys.specularReflection[0] * vars.specularLight[0] * speP;
                if (setG) g += d * objList[i]->pptys.specularReflection[1] * vars.specularLight[1] * speP;
                if (setB) b += d * objList[i]->pptys.specularReflection[2] * vars.specularLight[2] * speP;
            }
        }

        // secondary light
        {
            double incid = dot_prod( viewV, normal );
            double rr, rg, rb;
            double tr, tg, tb;
            double attn[3];

            // reflected
            if (incid > EPSZ && objList[i]->isSpecular())
            {
                Vector3 rayReflectionV = normal * 2 * incid - viewV;
                Ray refl = { intersection, rayReflectionV };

                rr = rg = rb = 0;
                rayTrace( refl, rr, rg, rb, mask, i, level + 1 );
            }

            // refracted
            double ts = objList[i]->pptys.opticalSpd;
            if (ts > EPSZ)
            {
                Vector3 transmisV;
                tr = tg = tb = 0;

                if (mask == SET_ALL)
                {
                    for (int c = 0; c <= 2; c++)
                    {
                        double tts = ts * (1 + (c - 1)*0.005);
                        if (incid > EPSZ) //into
                        {
                            transmisV = normal * (tts*incid - sqrt( 1 - tts*tts*(1 - incid*incid) )) - viewV * tts;
                        }
                        else //out
                        {
                            double invts = 1.0 / tts;
                            double det = 1 - invts*invts*(1 - incid*incid);
                            if (det < 0) return;
                            transmisV = normal * (-invts*incid - sqrt( det )) - viewV * invts;
                        }
                        transmisV.normalize();
                        Ray trns = { intersection, transmisV };

                        char m = c == 0 ? SET_RED : c == 1 ? SET_GRN : SET_BLU;
                        rayTrace( trns, tr, tg, tb, m, i, level + 1 );
                    }
                }
                else
                {
                    if (incid > EPSZ) //into
                    {
                        transmisV = normal * (ts*incid - sqrt( 1 - ts*ts*(1 - incid*incid) )) - viewV * ts;
                    }
                    else //out
                    {
                        double invts = 1.0 / ts;
                        double det = 1 - invts*invts*(1 - incid*incid);
                        if (det < 0) return;
                        transmisV = normal * (-invts*incid - sqrt( det )) - viewV * invts;
                    }
                    transmisV.normalize();
                    Ray trns = { intersection, transmisV };

                    rayTrace( trns, tr, tg, tb, mask, i, level + 1 );
                }

                double cAngle = incid > 0 ? incid : dot_prod( transmisV, normal );
                double R0 = ((ts - 1)*(ts - 1)) / ((ts + 1)*(ts + 1));
                attn[0] =
                attn[1] =
                attn[2] = R0 + (1 - R0)*pow( 1 - cAngle, 5 );

                if (setR) r += (1 - attn[0])*tr;
                if (setG) g += (1 - attn[1])*tg;
                if (setB) b += (1 - attn[2])*tb;
            }
            else
            {
                attn[0] = objList[i]->pptys.specularReflection[0];
                attn[1] = objList[i]->pptys.specularReflection[1];
                attn[2] = objList[i]->pptys.specularReflection[2];
            }

            if (setR) r += attn[0] * rr;
            if (setG) g += attn[1] * rg;
            if (setB) b += attn[2] * rb;
        }

        // photons
        {
            double pr, pg, pb;
            pr = pg = pb = 0;
            estimateRadiance( causticPhotonMap, intersection, pr, pg, pb );

            r += pr;
            g += pg;
            b += pb;
        }
    }
}

