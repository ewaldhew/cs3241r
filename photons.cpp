#include "photons.h"
#include "raytracer.h"
#include <algorithm>

#define INITIAL_GATHER_SQDIST 100.0
#define EXPOSURE 50.0 // number of photons at brightest pixel

void insertPhoton( std::vector<Photon>& photons,
                   Vector3 pos,
                   Vector3 const& dir,
                   Vector3 const& energy )
{
    Photon photon = new Photon_;

    photon->dir_p = (char)round(255 * (atan2( dir.x[1], dir.x[0] ) + M_PI) / (2 * M_PI));
    photon->dir_t = (char)round(255 * acos( dir.x[2] ) / M_PI);
    photon->e_r = (float)energy.x[0];
    photon->e_g = (float)energy.x[1];
    photon->e_b = (float)energy.x[2];
    photon->flag = false;
    photon->pos = pos;

    photons.push_back( photon );
}


void constructKDTree( KDTreeNode* root,
                      std::vector<Photon>::iterator begin,
                      std::vector<Photon>::iterator end )
{
    Vector3 min, max;

    if ( begin >= end )
    {
        root->data = 0;
        return;
    }
    if ( begin == end - 1 )
    {
        root->data = *begin;
        root->k = -1;
        return;
    }

    // find the cube surrounding the points
    min = max = (*begin)->pos;
    for (auto it = begin; it != end; ++it)
    {
        Photon photon = *it;
        for (int i = 0; i < 3; i++) {
            if (min.x[i] > photon->pos.x[i])
                min.x[i] = photon->pos.x[i];
            if (max.x[i] < photon->pos.x[i])
                max.x[i] = photon->pos.x[i];
        }
    }

    // select dimension in which the cube is largest
    int K = (max.x[0] - min.x[0]) > (max.x[1] - min.x[1]) ? 0 : 1;
    if (K < max.x[2] - min.x[2])
        K = 2;
    root->k = K;

    // find median of points in that dimension
    auto mid = begin + (end - begin) / 2;
    std::nth_element( begin, mid, end,
                      [K]( Photon const& a, Photon const& b )
                      { return a->pos.x[K] < b->pos.x[K]; } );

    root->data = *mid;

    root->left  = new KDTreeNode;
    root->right = new KDTreeNode;

    constructKDTree( root->left, begin, mid );
    constructKDTree( root->right, mid + 1, end );
}

void constructKDTreeHeap( KDTreeHeap& root,
                          int idx,
                          std::vector<Photon>::iterator begin,
                          std::vector<Photon>::iterator end )
{
    Vector3 min, max;

    if (begin >= end)
        return;
    if (begin == end - 1)
    {
        root.at( idx ) = *begin;
        return;
    }

    // find the cube surrounding the points
    min = max = (*begin)->pos;
    for (auto it = begin; it != end; ++it)
    {
        Photon photon = *it;
        for (int i = 0; i < 3; i++) {
            if (min.x[i] > photon->pos.x[i])
                min.x[i] = photon->pos.x[i];
            if (max.x[i] < photon->pos.x[i])
                max.x[i] = photon->pos.x[i];
        }
    }

    // select dimension in which the cube is largest
    int K = (max.x[0] - min.x[0]) > (max.x[1] - min.x[1]) ? 0 : 1;
    if (K < max.x[2] - min.x[2])
        K = 2;

    // find median of points in that dimension
    auto mid = begin + (end - begin) / 2;
    std::nth_element( begin, mid, end,
                      [K]( Photon const& a, Photon const& b )
                      { return a->pos.x[K] < b->pos.x[K]; } );

    root.at( idx ) = *mid;

    constructKDTreeHeap( root, 2 * idx, begin, mid );
    constructKDTreeHeap( root, 2 * idx + 1, mid + 1, end );
}

void drawKDTree( KDTreeNode* root, double r, double g, double b, int& count )
{
#ifdef DEBUG_
    std::vector<KDTreeNode*> reversed;
    std::vector<int> rlevel;

    std::vector<KDTreeNode*> stack;
    std::vector<int> level;
    stack.push_back( root );
    level.push_back( 1 );

    while ( !stack.empty() )
    {
        KDTreeNode *curr = stack.back();
        int clvl = level.back();
        stack.pop_back();
        level.pop_back();

        if ( curr->right && curr->right->data )
        {
            stack.push_back( curr->right );
            level.push_back( clvl + 1 );
        }
        if ( curr->left && curr->left->data )
        {
            stack.push_back( curr->left );
            level.push_back( clvl + 1 );
        }
        if ( curr->data )
        {
            reversed.push_back( curr );
            rlevel.push_back( clvl );
        }
    }

    while ( !reversed.empty() )
    {
        KDTreeNode *curr = reversed.back();
        int clvl = rlevel.back();
        reversed.pop_back();
        rlevel.pop_back();
        double d = 1.0 - (double)clvl / 10.0;
        if ( d < 0.1 ) d = 0.1;
        if ( drawPoint( curr->data->pos, r*d, g*d, b*d ) ) count++;
    }
#endif // DEBUG_
}


void makePhotonMap( KDTreeNode* map )
{
    std::vector<Photon> photons;

    //TODO: Actually test the tree!
    for (int i = 0; i < 50; i++)
    {
        //photons.push_back( new Photon );
        //insertPhoton( photons, Vector3( i - 25, 10*sin((double)i/10.0), -200.0 ), Vector3( 0, 0, 1 ), Vector3( i, i, i ) );
    }
    emitCausticPhotons( photons );
    std::cout << photons.size() << " photons" << std::endl;

    if ( photons.size() )
    {
        int count = 1;
        constructKDTree( map, photons.begin(), photons.end() );
        drawPoint( map->data->pos, 1.0, 0.0, 0.0 );
        drawKDTree( map->left, 0.7, 0.7, 0.0, count );
        drawKDTree( map->right, 0.0, 0.7, 1.0, count );
        std::cout << "found " << count << "photons" << std::endl;
    }
}


void gatherPhotons( KDTreeNode* root, PhotonGatherHeap& nearest, double& searchDistSq, Vector3 const& center )
{
    if ( !root || root->k < 0 ) return;

    // distance to splitting plane
    double dP = root->data->pos.x[root->k] - center.x[root->k];

    // search the side we are on first
    KDTreeNode *first  = dP < 0 ? root->right : root->left;
    KDTreeNode *second = dP < 0 ? root->left : root->right;

    gatherPhotons( first, nearest, searchDistSq, center );
    if ( dP*dP < searchDistSq )
    {
        gatherPhotons( second, nearest, searchDistSq, center );
    }

    // squared distance to center of search
    double dS = (root->data->pos - center).lengthsqr();
    if ( dS < searchDistSq )
    {
        nearest.insert( root->data, dS );
        if ( nearest.isFull() ) searchDistSq = nearest.getMaxDistSq();
    }
}


// Weight the energy of the current photon based on exposure settings and
// distance from the gather center (cone filter)
inline static double filterWeight( double dist )
{
    return (1.0 - dist / sqrt( INITIAL_GATHER_SQDIST )) / EXPOSURE;
}

// Compute radiance estimate at specified point
void estimateRadiance( KDTreeNode* map, Vector3 const& pos, double& r, double& g, double& b )
{
    PhotonGatherHeap nearest = PhotonGatherHeap( EXPOSURE );
    double temp = INITIAL_GATHER_SQDIST;

    gatherPhotons( map, nearest, temp, pos );

    std::vector<Photon> photons = nearest.getList();
    for ( Photon p : photons )
    {
        double weight = filterWeight( (p->pos - pos).length() );

        r += p->e_r * weight;
        g += p->e_g * weight;
        b += p->e_b * weight;
    }
}