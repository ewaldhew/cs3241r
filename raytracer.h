#ifndef RAYTRACER_H_
#define RAYTRACER_H_

#include "main.h"
#include "photons.h"

#define SET_ALL 0x7
#define SET_RED 0x4
#define SET_GRN 0x2
#define SET_BLU 0x1

struct Ray { // a ray that start with "start" and going in the direction "dir"
    Vector3 start;
    Vector3 dir;
};

class Cone {
public:
    Ray center;
    double tan_a;
    double inv_cos_a;

    Cone( Ray center, double angle ) {
        this->center = center;
        this->center.dir.normalize();
        tan_a = tan( angle );
        inv_cos_a = 1.0 / cos( angle );
    }
};

extern RtObject  **objList;
extern SceneVars   vars;
extern KDTreeNode *causticPhotonMap;

void rayTrace( Ray ray, double& r, double& g, double& b, char mask, int fromObj = -1, int level = 0 );
void emitCausticPhotons( std::vector<Photon>& photons );

#endif
